Frytter![image_alt_text](app/src/main/res/mipmap-mdpi/ic_launcher.png)
====



  Frytterは自動RT,自動ふぁぼ,ツイ消し感知,一括フォロー,等

  多くの機能を備えたTwitterクライアントです。
  
  


## Description

Frytterは多くの機能を備えたTwitterクライアントです

従来のクライアントにはない動作と美しさを目指して開発しています


##対象Androidバージョン
Android 4.0.3~

##動作確認済みデバイス
Arrows F-05D 4.2

Xperia Z SO-02e 4.4

Kindle Fire HD 4.1

GALAXY S3 4.2

AQUOS PHONE ZETA SH-02E 4.1.2

Xperia ZL2 SOL25 5.0.2


## ライセンス

This software is licensed under the MIT/X11 License.


See LICENSE.TXT for more detail.


Use this Software at your own risk.

Please note that we shall not be responsible for any loss, damages and troubles.


## Author

[WEBIFRY](https://twitter.com/HonoXNico)
