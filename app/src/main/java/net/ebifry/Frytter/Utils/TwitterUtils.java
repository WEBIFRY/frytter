package net.ebifry.Frytter.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.widget.Toast;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;
import net.steamcrafted.loadtoast.LoadToast;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

//RT
//RT削除
//ふぁぼ
//ただのツイート
//リプライ
//フォロー
//汎用

@EBean
public class TwitterUtils
{
    static LoadToast lt;
    //RT

    @Background
    public void ReTweet(long id) {
        try {
            Frytter.main_Twitter.retweetStatus(id);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    //RT
    @Background
    public void DestroyStatus(long id) {
        try {
            Frytter.main_Twitter.destroyStatus(id);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }
    //ふぁぼ
    @Background
    public void DisFavorite(long id) {
        try {
            Frytter.main_Twitter.destroyFavorite(id);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    //ふぁぼ

    @Background
    public void favorite(long id) {
        try {
            Frytter.main_Twitter.createFavorite(id);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }
    //ただのツイート
    @Background
    public   void TweetWithMedias(Activity activity,StatusUpdate text,ArrayList<File> fileArrayList)
    {

        RichToastShow(activity.getString(R.string.Sending),activity);
        try
        {
            //StatusUpdate statusUpdate=new StatusUpdate(text);
           // Frytter.main_Twitter.updateStatus(statusUpdate);
            RichToastDis(true);
        }
        catch (Exception e)
        {
            System.out.print(e.getMessage());
            e.printStackTrace();
            RichToastDis(false);
        }
    }

    //ただのツイート
    @Background
    public   void Tweet(Activity activity,String text)
    {

        RichToastShow(activity.getString(R.string.Sending),activity);
        try
        {
            StatusUpdate statusUpdate=new StatusUpdate(text);
            Frytter.main_Twitter.updateStatus(statusUpdate);
            RichToastDis(true);
        }
        catch (TwitterException e)
        {
            System.out.print(e.getMessage());
            e.printStackTrace();
            RichToastDis(false);
        }
    }

    //リプライ

    @Background
    public void ReplyTweet(Activity activity,long ID,String text)
    {

        RichToastShow(activity.getString(R.string.Sending),activity);
        try
        {
            StatusUpdate statusUpdate=new StatusUpdate(text);
            statusUpdate.setInReplyToStatusId(ID);
            Frytter.main_Twitter.updateStatus(statusUpdate);
            RichToastDis(true);
        }
        catch (TwitterException e)
        {
            e.printStackTrace();
            RichToastDis(false);
        }
    }
//フォロー

    @Background
    public void Follow(String userName, Twitter twitter, Activity activity) {
        try {
            List<User> target = Frytter.main_Twitter.lookupUsers(userName);
            twitter.createFriendship(target.get(0).getId());
        } catch (TwitterException e) {
            e.printStackTrace();
        }

    }




    //遡り
    @Background
    public void goBackUser(String screen_name, int count) {

        List<Status> statuses = null;
        try {
            statuses = Frytter.main_Twitter.getUserTimeline(screen_name, new Paging(1, count));
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        if (statuses != null) {
            for (Status status : statuses) {
                long id = status.getId();
                favorite(id);
            }
        }

    }
    //汎用
    @UiThread
    public void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
    @UiThread
    public void RichToastShow(String string, Activity activity)
    {
        if (lt == null) {
            lt=new LoadToast(activity);
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            lt = new LoadToast(activity);
            lt.setText(string);
            lt.setTranslationY(size.y / 2);
            lt.show();
        }

    }

    @UiThread
    void RichToastDis(boolean show)
    {
        if (lt != null) {
            if (show) {
                lt.success();
            } else {
                lt.error();
            }
            lt = null;
        }
    }

}