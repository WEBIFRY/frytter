package net.ebifry.Frytter.Utils;

import android.content.Context;
import android.support.annotation.Nullable;

import net.ebifry.Frytter.Declaration.SharedPrefs_;
import net.ebifry.Frytter.model.DBAccount;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import io.realm.Realm;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;

@EBean
public class OAuthUtils {
    private Realm realm;
    @Pref
    SharedPrefs_ myPrefs;
    //メインはこっち
    public  Twitter getTwitterInstance(Context context) {
        String consumerKey = myPrefs.ConsumerKey().getOr(null);
        String consumerSecret =myPrefs.ConsumerSecret().getOr(null);
        TwitterFactory factory = new TwitterFactory();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer(consumerKey, consumerSecret);
        if (hasAccessToken(context)) {
            twitter.setOAuthAccessToken(loadAccessToken(context));
        }
        return twitter;
    }

    //サブ垢のインスタンス
    public Twitter getTwitterInstances(Context context, String name) {
        String consumerKey = myPrefs.ConsumerKey().getOr(null);
        String consumerSecret =myPrefs.ConsumerSecret().getOr(null);
        TwitterFactory factory = new TwitterFactory();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer(consumerKey, consumerSecret);
        realm = Realm.getInstance(context);
        DBAccount account = realm.where(DBAccount.class).equalTo("Name", name).findFirst();
        twitter.setOAuthAccessToken(new AccessToken(account.getKey(), account.getSecret()));
        realm.close();
        return twitter;

    }
    //TwitterStreamのメインのインスタンス
    public  TwitterStream getTwitterStreamInstance(Twitter t) {
        return new TwitterStreamFactory().getInstance(t.getAuthorization());
    }

    public void storeAccessToken(Context context, AccessToken accessToken) {
        realm = Realm.getInstance(context);
        realm.beginTransaction();
        DBAccount account = realm.createObject(DBAccount.class);
        account.setKey(accessToken.getToken());
        account.setSecret(accessToken.getTokenSecret());
        account.setID(accessToken.getUserId());
        account.setName(accessToken.getScreenName());
        realm.commitTransaction();
        realm.close();

    }


    @Nullable
    public  AccessToken loadAccessToken(Context context) {
        realm = Realm.getInstance(context);

        DBAccount dbAccount;
        long id = myPrefs.Main().getOr(1L);
        if(id!= 1L){
            dbAccount = realm.where(DBAccount.class).equalTo("ID", id).findFirst();

        } else {
            dbAccount = realm.where(DBAccount.class).findFirst();

        }
        if (dbAccount != null) {
            String token = dbAccount.getKey();
            String tokenSecret = dbAccount.getSecret();
            if (token != null && tokenSecret != null) {

                realm.close();
                return new AccessToken(token, tokenSecret);

            } else {
                realm.commitTransaction();
                realm.close();
                return null;
            }


        }
        return null;
    }

    public  boolean hasAccessToken(Context context) {
        return loadAccessToken(context) != null;}

}
