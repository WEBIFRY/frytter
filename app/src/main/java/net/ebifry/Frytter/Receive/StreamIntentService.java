package net.ebifry.Frytter.Receive;

import android.app.IntentService;
import android.content.Intent;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.Fragments.TL;
import net.ebifry.Frytter.Frytter_;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.User;
import twitter4j.UserStreamAdapter;


@EIntentService
public class StreamIntentService extends IntentService {
    public  static  TwitterStream stream;
    public  static TL TL;


    public StreamIntentService() {
        super("StreamIntentService");}

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @ServiceAction
    void  TL() {

        stream.addListener(new TweetNotification());
        stream.addListener(new
                                   StatusListener() {
                                       @Override
                                       public void onStatus(Status status) {
                                           TL.AddAdapter(status);

                                           if (status.getInReplyToScreenName().equals(Frytter.myName))
                                           {
                                               Intent broadcastIntent = new Intent(getApplicationContext(),NotificationBroadCast_.class);
                                               broadcastIntent.putExtra("type", "reply");
                                               broadcastIntent.putExtra("sourceName", status.getUser().getName());
                                               broadcastIntent.putExtra("text",status.getText());
                                               broadcastIntent.putExtra("ID",status.getId());
                                               sendBroadcast(broadcastIntent);
                                           }
                                       }

                                       @Override
                                       public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                                           TL.DeleteAdapter(statusDeletionNotice.getStatusId());
                                       }

                                       @Override
                                       public void onTrackLimitationNotice(int i) {

                                       }

                                       @Override
                                       public void onScrubGeo(long l, long l1) {

                                       }

                                       @Override
                                       public void onStallWarning(StallWarning stallWarning) {

                                       }

                                       @Override
                                       public void onException(Exception e) {

                                       }

                                   }

        );

        stream.user();



    }
    class TweetNotification extends UserStreamAdapter {
        @Override
        public final void onFavorite(User source, User target, Status favoritedStatus) {
            //ふぁぼ追加
            //通知
            Intent broadcastIntent = new Intent(getApplicationContext(),NotificationBroadCast_.class);
            broadcastIntent.putExtra("type", "favorite");
            broadcastIntent.putExtra("sourceName", source.getName());
            broadcastIntent.putExtra("sourceScreenName", source.getScreenName());
            broadcastIntent.putExtra("target", target.getName());
            broadcastIntent.putExtra("text", favoritedStatus.getText());
            broadcastIntent.putExtra("ID",favoritedStatus.getId());
             sendBroadcast(broadcastIntent);
        }



        public void onUnfollow(User source, User unfollowedUser) {
        }



    }


}

