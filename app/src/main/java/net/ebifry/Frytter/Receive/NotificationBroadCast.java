package net.ebifry.Frytter.Receive;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.SystemService;

@EReceiver
public class NotificationBroadCast extends BroadcastReceiver  {
    int notificationCountF=0;
    int notificationCountR = 0;
    @SystemService
    NotificationManager manager;
    @Override
        public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String type = bundle.getString("type");
        String APP_NAME= context.getString(R.string.app_name);
        if (Frytter.sharedPreferences.getBoolean("enable_notification", true) && type != null) {
            switch (type) {
                case "favorite":
                    String sourceScreen = bundle.getString("sourceScreenName");
                    if (sourceScreen != null) {
                        if (Frytter.sharedPreferences.getBoolean("enable_notification_favorite", true) && !sourceScreen.equals(Frytter.myName)) {

                            Notification.Builder builder = new Notification.Builder(context);
                            builder.setDefaults(Notification.DEFAULT_ALL);
                            builder.setContentText( String.format(  "%sからふぁぼ",bundle.getString("sourceName")));
                            builder.setTicker(String.format(  "%sからふぁぼ",bundle.getString("sourceName")));
                            builder.setSubText(bundle.getString("text"));
                            builder.setSmallIcon(R.mipmap.ic_launcher);
                            builder.setContentTitle(APP_NAME);
                            builder.setAutoCancel(true);
                            builder.setNumber(notificationCountF);
                            manager.notify(1, builder.build());
                            ++notificationCountF;
                        }
                    }
                    break;
                case "reply":
                    if (Frytter.sharedPreferences.getBoolean("enable_notification_reply", true)) {
                        Notification.Builder builder = new Notification.Builder(context);
                        builder.setAutoCancel(true);
                        builder.setSmallIcon(R.mipmap.ic_launcher);
                        builder.setContentTitle(APP_NAME);
                        builder.setContentText(String.format(  "%sから返信",bundle.getString("sourceName")) );
                        builder.setSubText(bundle.getString("text"));
                        builder.setDefaults(Notification.DEFAULT_ALL);
                        manager.notify(2, builder.build());
                        ++notificationCountR;
                    }break;

        }


    }




}}
