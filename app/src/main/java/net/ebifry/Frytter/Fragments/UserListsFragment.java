package net.ebifry.Frytter.Fragments;


import android.app.Fragment;
import android.support.annotation.NonNull;
import android.widget.AbsListView;
import android.widget.ListView;

import net.ebifry.Frytter.Adapter.ListAdapter;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import twitter4j.PagableResponseList;
import twitter4j.TwitterException;
import twitter4j.UserList;

@EFragment(R.layout.fragment_user_list)
public class UserListsFragment extends Fragment
{
    @FragmentArg
    String Source;

    @ViewById(R.id.UserList)
    ListView listView;

    ListAdapter mAdapter;

    long cursor=-1L;

    @AfterViews
    void Initialize()
    {

        mAdapter=new ListAdapter(getActivity());
        ReloadList();
        listView.setAdapter(mAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
           @Override
           public void onScrollStateChanged(AbsListView view, int scrollState) {
           }

           @Override
           public void onScroll(@NonNull AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
               if (view.getChildCount() != 0) {
                   if (view.getLastVisiblePosition() == totalItemCount - 1
                           && view.getChildAt(view.getChildCount() - 1).getBottom() <= view.getBottom() - view.getPaddingBottom()) {
                       ReloadList();
                   }
               }

           }
       });


    }
    @Background
    void ReloadList(){try {
        setAdapter(Frytter.main_Twitter.getUserListMemberships(Source,cursor));
    } catch (TwitterException e) {
        e.printStackTrace();
    }}
    @UiThread
    void setAdapter(PagableResponseList<UserList> list)
    {
        for (UserList userlist :list)
        {
            mAdapter.add(userlist);
        }
        cursor=list.getNextCursor();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        listView=null;
       mAdapter=null;
    }
}
