package net.ebifry.Frytter.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import net.ebifry.Frytter.Activity.MainActivity_;
import net.ebifry.Frytter.Declaration.SharedPrefs_;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Utils.OAuthUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

@EFragment(R.layout.fragment_twitter_oauth)
public class TwitterOAuth extends DialogFragment {
    @Pref
    SharedPrefs_ mPref;
    @Bean
    OAuthUtils oauthUtils;
    @StringRes(R.string.twitter_callback_url)
   String mCallbackURL;
    @ViewById(R.id.webView)
    WebView web ;
    private ProgressDialog waitDialog;
    private Twitter mTwitter;
    private RequestToken mRequestToken;
    private Activity parent;

    @Override
    public void onAttach(Activity activity) {
        parent = activity;
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        if (mPref.Initial().getOr(true)) parent.finish();
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
    @AfterViews
    void set(){
        mTwitter  = new TwitterFactory().getInstance();
        mTwitter.setOAuthConsumer(mPref.ConsumerKey().getOr(null), mPref.ConsumerSecret().getOr(null));
        OAuth();
    }
    @Background
    void OAuth()
    {
       try{ mRequestToken = mTwitter.getOAuthRequestToken(mCallbackURL);}
       catch (Exception e){e.printStackTrace();}
        OAuthExecute(mRequestToken.getAuthorizationURL());
    }
  @UiThread
  void OAuthExecute(String url)
  {

      if (url != null) {
          web.loadUrl(url);
          web.setWebViewClient(new WebViewClient() {
              @Override
              public boolean shouldOverrideUrlLoading(WebView view, String callBackUrl) {
                  if (callBackUrl.startsWith("webi://twitter")) {
                      view.stopLoading();
                      Call(Uri.parse(callBackUrl));
                      return true;
                  }
                  return false;
              }

              @Override
              public void onPageFinished(WebView view, String callBackUrl) {
                  if (waitDialog != null) {
                      waitDialog.dismiss();
                      waitDialog = null;
                  }}}
          ); }}




    @Background
    void Call(Uri uri){
        String verifier = uri.getQueryParameter("oauth_verifier");
                try {
                    callBack(mTwitter.getOAuthAccessToken(mRequestToken, verifier));
                } catch (TwitterException e) {
                    e.printStackTrace();
                }}

  @UiThread
  void callBack(AccessToken accessToken)
  {
      if (accessToken != null) {
          showToast("OK!(･ω･つ)３");
          successOAuth(accessToken);
      } else {

          showToast("失敗(･ω･つ)３");
      }
  }
    private void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }
    private void successOAuth(AccessToken accessToken) {
        oauthUtils.storeAccessToken(getActivity(), accessToken);
        if (mPref.Initial().getOr(true)) {
            MainActivity_.intent(this).start();
            mPref.Initial().put(false);
            parent.finish();
        }
        this.dismiss();
    }

}
