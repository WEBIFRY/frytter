package net.ebifry.Frytter.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import net.ebifry.Frytter.Activity.UserActivity_;
import net.ebifry.Frytter.Declaration.CircleTransform;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.concurrent.ExecutionException;

import twitter4j.TwitterException;
import twitter4j.User;

@EFragment(R.layout.fragment_user_detail)
public class UserDetailFragment extends Fragment
{

    FragmentManager fm;
    @ViewById(R.id.Button_Tweet)
    BootstrapButton tweet;
    @ViewById(R.id.button_following)
    BootstrapButton following;
    @ViewById(R.id.button_followed)
    BootstrapButton followed;
    @ViewById(R.id.button_favorite)
    BootstrapButton favorite;
    @ViewById(R.id.button_listed)
    BootstrapButton listed;
    @ViewById(R.id.button_ratio)
    BootstrapButton ratio;
    @ViewById(R.id.profile_banner)
    ImageView profile_banner;
    @ViewById(R.id.imageView_profile)
    BootstrapCircleThumbnail profile_img;
    @ViewById(R.id.textView_UserName)
    TextView userName;
    @ViewById(R.id.textView_screenName)
    TextView screenName;
    @ViewById(R.id.textView_description)
    TextView textView_description;

   UserActivity_ activity;

    @FragmentArg("UserName")
    String UserID;

    User user;



    @Background
    @AfterViews
    void Initialize()
    {
        tweet.setEnabled(false);
        ratio.setEnabled(false);
        following.setEnabled(false);
        followed.setEnabled(false);
        favorite.setEnabled(false);
        listed.setEnabled(false);
        try {
            User user = Frytter.main_Twitter.showUser(UserID);
          //  isMe = user.getScreenName().equals(Frytter.main_Twitter.getScreenName());
            InitializeExecute(user);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }


    @UiThread
    void InitializeExecute(final User user_)
    {
       user=user_;
    fm=activity.getSupportFragmentManager();
        if(user_!=null){
            String description=user_.getDescription() +
                    "\n"+getString(R.string.user_activity_location, user_.getLocation())+
                    "\n"+getString(R.string.user_activity_web,user_.getURLEntity().getExpandedURL());

            //画像読み込み
            Glide.with(getActivity())
                    .load(user_.getOriginalProfileImageURLHttps())
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            profile_img.setImage(resource);
                        }
                    });
            Glide.with(getActivity())
                    .load(user_.getProfileBannerIPadRetinaURL())
                    .into(profile_banner);
            userName.setText(user_.getName());
            screenName.setText(getString(R.string.ScreenName, user_.getScreenName()));
            tweet.setText(user_.getStatusesCount() + "\nTweet");
            following.setText(user_.getFriendsCount() + "\nFollow");
            followed.setText(user_.getFollowersCount() + "\nFollower");
            favorite.setText(user_.getFavouritesCount() + "\nFav");
            listed.setText(user_.getListedCount() + "\nListed") ;
            textView_description.setText(description);
            tweet.setEnabled(true);
            ratio.setEnabled(true);
            following.setEnabled(true);
            followed.setEnabled(true);
            favorite.setEnabled(true);
            listed.setEnabled(true);
            double a = user_.getFollowersCount();
            double b = user_.getFriendsCount();
            double c = (a / b) * 10;
            double d = Math.round(c);
            double e = d / 10;
            ratio.setText(e + "\nFF");
        }
    }

    @Override
    public void onAttach(Activity activity_) {
    super.onAttach(activity_);
    activity=(UserActivity_)activity_;
}


    @Click(R.id.imageView_profile)
    void btzOnClick(){ PictureFragment_.builder().URL(user.getOriginalProfileImageURLHttps()).build().show(fm, "");}
    @Click(R.id.profile_banner)
    void btqOnClick(){ PictureFragment_.builder().URL(user.getProfileBannerMobileRetinaURL()).build().show(fm, "");}
    @Click(R.id.Button_Tweet)
    void btfOnClick(){activity.fragmentReplacer("Tweet",screenName.getText().toString());}
    @Click(R.id.button_followed)
    void btcOnClick(){activity.fragmentReplacer("Followed",screenName.getText().toString());}
    @Click(R.id.button_following)
    void btOnClick(){activity.fragmentReplacer("Following",screenName.getText().toString());}
    @Click(R.id.button_favorite)
    void bthOnClick(){activity.fragmentReplacer("Favorite",screenName.getText().toString());}
    @Click(R.id.button_listed)
    void btjOnClick(){activity.fragmentReplacer("List",screenName.getText().toString());}
    @Click(R.id.button_ratio)
    void btkOnClick(){activity.fragmentReplacer("Ratio",screenName.getText().toString());}
}
