package net.ebifry.Frytter.Fragments;


import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.AbsListView;
import android.widget.ListView;

import net.ebifry.Frytter.Adapter.UserAdapter;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import twitter4j.PagableResponseList;
import twitter4j.TwitterException;
import twitter4j.User;

@EFragment(R.layout.fragment_user_list)
public class UserListFragment extends Fragment
{
    @FragmentArg
    String Source;

    @ViewById(R.id.UserList)
    ListView listView;

    @FragmentArg
    String Mode;

    UserAdapter mAdapter;
    long cursor = -1L;

    @AfterViews
    void Initialize()
    {
        mAdapter = new UserAdapter(getActivity());
        ReloadUsers();
        listView.setAdapter(mAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(@NonNull AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getChildCount() != 0) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1
                            && view.getChildAt(view.getChildCount() - 1).getBottom() <= view.getBottom() - view.getPaddingBottom()) {
                        ReloadUsers();
                    }
                }

            }
        });
    }

    @Background
    void ReloadUsers() {
        {
            try { switch (Mode) {
                case "Following":

                        AddAdapter(Frytter.main_Twitter.getFriendsList(Source, cursor));

                    break;
                case "Followed":
                        AddAdapter(Frytter.main_Twitter.getFollowersList(Source, cursor));
                    break;



            } } catch (TwitterException e) {
                e.printStackTrace();
            }}}

            @UiThread
            void AddAdapter (PagableResponseList<User> list)
            {
                cursor = list.getNextCursor();
                for (User user : list) {
                    mAdapter.add(user);
                }
            }

        }


