package net.ebifry.Frytter.Fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;


import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.co.senab.photoview.PhotoView;

@EFragment(R.layout.fragment_picture)
public class PictureFragment extends DialogFragment {
    @FragmentArg("url")
    String URL;
    @FragmentArg("isGif")
    boolean isGif;
    @ViewById(R.id.imageView_pic)
    PhotoView img;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        return dialog;
    }

    @AfterViews
    void set()
    {

       if(isGif)
       {
           Glide
                   .with(this)
                   .load(URL)
              .asGif().into(new SimpleTarget<GifDrawable>() {
               @Override
               public void onResourceReady(GifDrawable resource, GlideAnimation<? super GifDrawable> glideAnimation) {
                   img.setImageDrawable(resource);
               }
           });
       }
        else
       {
           Glide
                   .with(this)
                   .load(URL)
                   .into(new SimpleTarget<GlideDrawable>() {
                       @Override
                       public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                           img.setImageDrawable(resource);
                       }
                   });
       }


    }


  @LongClick(R.id.imageView_pic)
  void onLongClick() {
      final Date date = new Date();
      final  SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_HH_mm_ss",Locale.getDefault());

        Glide.with(getActivity()).load(URL).asBitmap().into(new SimpleTarget<Bitmap>() {
                                                                 @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation)
            {
                File dataDir;
                dataDir = new File(Environment.getExternalStorageDirectory(),getActivity().getString(R.string.picture_download_location) );
                File filePath=new File(dataDir,sdf.format(date) + ".jpg");
                try {
                    OutputStream os;
                    os = new FileOutputStream(filePath);
                    resource.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.close();
                    showToast("ダウンロード完了");
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast("ダウンロード失敗");
                }

            }
                    }
        );



}


@UiThread
  void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }


}
