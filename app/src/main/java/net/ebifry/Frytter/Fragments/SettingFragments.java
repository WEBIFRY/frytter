package net.ebifry.Frytter.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

@EFragment
public class SettingFragments extends PreferenceFragment {
    @FragmentArg("PreferenceScreen")
    String string;

    private static void launchChooser(Activity context) {

        Intent intent = new Intent();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.setType(context.getString(R.string.intent_type_image));

        context.startActivityForResult(intent, 0);
    }

    @AfterViews
     void OnCreate() {
        if(string!=null){
        switch (string){
            case "Notification":
                addPreferencesFromResource(R.xml.pref_notification);
                break;
            case "Design":
                addPreferencesFromResource(R.xml.pref_design);
                Preference button = findPreference("chooser");
                button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference pref) {
                        launchChooser(getActivity());
                        return true;
                    }
                });
                break;
    }}}
}
