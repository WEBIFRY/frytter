package net.ebifry.Frytter.Fragments;


import android.media.session.MediaController;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.VideoView;

import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

@EFragment
public class VideoViewFragment extends DialogFragment {

    @FragmentArg("url")
    String URL;
    @ViewById(R.id.videoView)
    VideoView videoView;

@AfterViews
    void init()
{
    Toast.makeText(getActivity(),URL,Toast.LENGTH_LONG).show();
// インターネット上のファイルを再生
    videoView.setVideoURI(Uri.parse(URL));
    videoView.start();
    videoView.setMediaController(new android.widget.MediaController(getActivity()));
}


}
