package net.ebifry.Frytter.Fragments;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.model.DBAccount;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import io.realm.Realm;

@EFragment(R.layout.account_manage)
public class AccountsManagerFragment extends Fragment
{
@ViewById(R.id.account_list)
    ListView listView;



    @ItemClick(R.id.account_list)
    final void onItemClick(int position) {
        if (position == 0) {
            TwitterOAuth_ OAuth = new TwitterOAuth_();
            OAuth.show(getFragmentManager(), "");
        } else {
            final String item = (String) listView.getItemAtPosition(position);
            new AlertDialog.Builder(getActivity())
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle("このアカウントを削除します")
                    .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                        public void onClick(@NonNull DialogInterface dialog, int whichButton) {
                            List<DBAccount> list2 = Frytter.realm.where(DBAccount.class).equalTo("Name", item).findAll();
                            Frytter.realm.beginTransaction();
                            list2.clear();
                            Frytter.realm.commitTransaction();
                            Toast.makeText(getActivity(), getString(R.string.isDeleted, item), Toast.LENGTH_LONG).show();
                        }

                    }).setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                public void onClick(@NonNull DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            })
                    .show();


        }

    }





@AfterViews
final void set()
{
    Realm realm=Realm.getInstance(getActivity());
    List<DBAccount> list2 = realm.where(DBAccount.class).findAll();

    //String型のリストを作る
    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1);
    adapter.add(getString(R.string.add_account));
    for (DBAccount i : list2)
    {
        adapter.add(i.getName());
    }
    //リストをFinalに宣言
    listView.setAdapter(adapter);
    realm.close();
    // リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録





}
}







