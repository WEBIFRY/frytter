package net.ebifry.Frytter.Fragments;


import android.app.Activity;
import android.app.Fragment;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.ebifry.Frytter.Activity.InitialActivity_;
import net.ebifry.Frytter.Declaration.SharedPrefs_;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import twitter4j.TwitterException;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.ConfigurationContext;

@EFragment(R.layout.fragment_consumer)
public class Consumer extends Fragment {
    InitialActivity_ parent;
    String consumer;
    String secret;
    @Pref
    SharedPrefs_ myPrefs;
    @ViewById(R.id.editText_consumer)
    EditText editText_consumer;
    @ViewById(R.id.editText_secret)
    EditText editText_secret;
    @ViewById(R.id.button_default_key)
    Button button;
    @Click(R.id.button_default_key)
    void DefStart() {
        button.setEnabled(false);
        consumer = getString(R.string.twitter_consumer_key);
        secret = getString(R.string.twitter_consumer_secret);
        getRequestToken();
    }

    @Click(R.id.button_done)
    void start() {
        button.setEnabled(false);
        consumer = editText_consumer.getText().toString();
        secret = editText_secret.getText().toString();
        getRequestToken();
    }

    @Background
    void getRequestToken() {

        OAuthAuthorization oauth = new OAuthAuthorization(ConfigurationContext.getInstance());
        oauth.setOAuthConsumer(consumer, secret);
        try {
            oauth.getOAuthRequestToken(getString(R.string.twitter_callback_url));
            getOAuthExecuted(true);

        } catch (TwitterException e) {
            getOAuthExecuted(false);
        }
    }

    @UiThread
    void getOAuthExecuted(boolean isCorrect) {
        if (isCorrect) {
            myPrefs.ConsumerKey().put(consumer);
            myPrefs.ConsumerSecret().put(secret);
            parent.OAuthSuccess();
        } else {
            showToast(getString(R.string.Error));
        }
    }

    private void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAttach(Activity activity) {
        parent = (InitialActivity_) activity;
        super.onAttach(activity);
    }
}
