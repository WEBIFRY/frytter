package net.ebifry.Frytter.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;

@EFragment(R.layout.fragment_tools)
public class ToolsFragment extends Fragment implements AdapterView.OnItemClickListener{
    public OnMenuItemSelectedListener mCallback;
    @ViewById(R.id.listView3)
    ListView lv2 ;
    @StringArrayRes(R.array.tools)
    String[] items;

    @Override
    public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
        String text = (String) parent.getItemAtPosition(position);
        mCallback.onMenuItemSelected(position, text);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnMenuItemSelectedListener) {
            mCallback = (OnMenuItemSelectedListener) activity; // 親Activityとの紐付け
        }
    }
    public interface OnMenuItemSelectedListener {
        void onMenuItemSelected(int position, String text);
    }

   @AfterViews
    public void set()
   {
       ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_expandable_list_item_1, items);
       lv2.setAdapter(adapter2);
       lv2.setOnItemClickListener(this);
   }




}
