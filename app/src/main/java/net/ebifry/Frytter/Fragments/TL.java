package net.ebifry.Frytter.Fragments;


import android.content.DialogInterface;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import net.ebifry.Frytter.Activity.AnyUserTweet_;
import net.ebifry.Frytter.Adapter.TweetAdapter;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Utils.TwitterUtils;
import net.steamcrafted.loadtoast.LoadToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.TwitterException;

@EFragment(R.layout.fragment_tl)
public class TL extends Fragment  {
    TweetAdapter mAdapter;
    int page=1;
    static LoadToast lt;
    @ViewById(R.id.listView4)
    ListView TL;
    @Bean
    TwitterUtils twitterUtils;
    @FragmentArg
    String type;
    @FragmentArg
    String source;
//    @StringArrayRes(R.array.tweet)
//    String[] items;
//    @ItemClick(R.id.listView4)
//    void click(int position)
//    {
//        // リスト表示用のアラートダイアログ
//        Status status= (Status) TL.getItemAtPosition(position);
//        if (status.isRetweet()) {status=status.getRetweetedStatus();}
//        final long id=status.getId();
//        final AlertDialog.Builder listDlg = new AlertDialog.Builder(getActivity());
//        listDlg.setTitle("Tweet");
//        listDlg.setItems(
//                items,
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        String selected=items[which];
//                        switch (selected)
//                        {
//                            case "詳細":
//                                AnyUserTweet_.intent(getContext()).statusID(id).start();
//                            break;
//                        }
//                    }
//                });
//
//        // 表示
//        listDlg.create().show();
//    }
    //ListView追加
    @UiThread
    public void AddAdapter(Status status)
    {
        mAdapter.insert(status, 0);
    }

    @UiThread
    public void DeleteAdapter(long tweetId)
    {
            
    }


    @AfterViews
    void Instances()
    {
        if(source!=null){TL.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.white));}
        mAdapter = new TweetAdapter(getActivity());
        TL.setAdapter(mAdapter);
        ReloadTimeLine();
        TweetAdapter.twitterUtils_ = twitterUtils;
        TweetAdapter.fm =getFragmentManager();
        TL.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(@NonNull AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getChildCount() != 0) {
                    if (view.getLastVisiblePosition() == totalItemCount - 1
                            && view.getChildAt(view.getChildCount() - 1).getBottom() <= view.getBottom() - view.getPaddingBottom()) {
                        ReloadTimeLine();
                    }
                }

            }
        });
    }

    void ReloadTimeLine()
    {

        Paging paging = new Paging(page, 30);
        page++;
        ReloadTimeLine(paging, type);
    }

    @Background
    public void ReloadTimeLine(Paging paging, String mode) {
        RichToastShow(getActivity().getString(R.string.Loading));
        try {
            switch (mode) {
                case "Favorite":
                    show( mAdapter, Frytter.main_Twitter.getFavorites(source,paging));
                    break;
                case "Tweet":
                    show( mAdapter, Frytter.main_Twitter.getUserTimeline(source,paging));
                    break;
                case "Me":
                    show( mAdapter, Frytter.main_Twitter.getUserTimeline(paging));
                    break;
                case "Mention":
                    show( mAdapter, Frytter.main_Twitter.getMentionsTimeline(paging));
                    break;
                case "Home":
                    show( mAdapter, Frytter.main_Twitter.getHomeTimeline(paging));
                    break;
            }

        } catch (TwitterException e) {
            RichToastDis(false);
        }

    }
    @UiThread
    void show(TweetAdapter adapter, ResponseList<Status> homeTimeline) {
        if (homeTimeline != null) {
            for (twitter4j.Status status : homeTimeline)
            {
                adapter.add(status);
            }
            RichToastDis(true);
        } else RichToastDis(false);
    }

    @UiThread
    public void RichToastShow(String string)
    {
        if (lt == null) {
            lt=new LoadToast(getActivity());
            lt.setProgressColor(ContextCompat.getColor(getContext(),R.color.primary));
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            lt.setText(string);
            lt.setTranslationY(size.y / 2);
            lt.show();
        }

    }

    @UiThread
    void RichToastDis(boolean show)
    {
        if (lt != null) {
            if (show) {
                lt.success();
            } else {
                lt.error();
            }
            lt = null;
        }
    }



}

