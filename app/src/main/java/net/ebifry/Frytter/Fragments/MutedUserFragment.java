package net.ebifry.Frytter.Fragments;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.model.DBMuteUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EFragment(R.layout.fragment_muted_user)
public class MutedUserFragment extends Fragment {
    @ViewById(R.id.listView5)
    ListView lv;


    @AfterViews
    void set() {
        List<DBMuteUser> list2 = Frytter.realm.where(DBMuteUser.class).findAll();
        //String型のリストを作る
        final ArrayAdapter<String> mute = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        //0番は追加のためのボタン、以降アカウント名
        mute.add(getString(R.string.fragment_muted_add_mute_user));
        for (DBMuteUser i : list2) {
            mute.add(i.getName());
        }
        //リストをFinalに宣言
        lv.setAdapter(mute);

    }

    @ItemClick(R.id.listView5)
    void OnClick(int position) {
        String item = (String) lv.getItemAtPosition(position);
        if (position == 0) {
            final EditText editView = new EditText(getActivity());
            new AlertDialog.Builder(getActivity())
                    .setIcon(android.R.drawable.ic_input_add)
                    .setTitle(getString(R.string.fragment_muted_set_mute_screen_name))
                    .setView(editView)
                    .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Frytter.realm.beginTransaction();
                            DBMuteUser muteUser = Frytter.realm.createObject(DBMuteUser.class);
                            muteUser.setName(editView.getText().toString());
                            Frytter.realm.commitTransaction();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    })
                    .show();
        } else {
            List<DBMuteUser> ist = Frytter.realm.where(DBMuteUser.class).equalTo("Name", item).findAll();
            Frytter.realm.beginTransaction();
            ist.clear();
            Frytter.realm.commitTransaction();
            Toast.makeText(getActivity(), String.format(  "%sを削除しました",item), Toast.LENGTH_LONG).show();
        }
    }
}







