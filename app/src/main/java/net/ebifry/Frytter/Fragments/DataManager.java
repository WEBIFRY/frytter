package net.ebifry.Frytter.Fragments;


import android.app.Fragment;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import net.ebifry.Frytter.Declaration.SharedPrefs_;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.model.DBAccount;
import net.ebifry.Frytter.model.DBMuteUser;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

@EFragment(R.layout.fragment_data_manager)
public class DataManager extends Fragment
{
    @Pref
    SharedPrefs_ mPrefs;
    @StringArrayRes(R.array.data_delete)
    String[] lists;
    @ViewById(R.id.data_delete)
    ListView listView;
    @AfterViews
    void set()
    {
        ArrayAdapter<String>  arrayAdapter=new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(lists);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull AdapterView<?> parent, @NonNull View view, int position, long id) {
                String text = (String) parent.getItemAtPosition(position);
                switch (text)
                {
                    case "背景を元に戻す":
                        mPrefs.Background().remove();
                        Frytter.activity.setBack(null);
                        break;
                    case "ミュート設定を全てリセット":
                        Frytter.realm.beginTransaction();
                        Frytter.realm.where(DBMuteUser.class).findAll().clear();
                        Frytter.realm.commitTransaction();
                        break;
                    case "アカウントを全てリセット":
                        Frytter.realm.beginTransaction();
                        Frytter.realm.where(DBAccount.class).findAll().clear();
                        Frytter.realm.commitTransaction();

                        break;
                    case "全ての設定をリセット":
                        Frytter.realm.beginTransaction();
                        Frytter.realm.where(DBMuteUser.class).findAll().clear();
                        Frytter.realm.where(DBAccount.class).findAll().clear();
                        Frytter.realm.commitTransaction();
                        mPrefs.clear();
                    break;
                }





}});}}
