package net.ebifry.Frytter.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.MediaColumns;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.beardedhen.androidbootstrap.FontAwesomeText;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Utils.TwitterUtils;
import net.steamcrafted.loadtoast.LoadToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import twitter4j.StatusUpdate;
import twitter4j.UploadedMedia;

@EActivity(R.layout.activity_tweet)
public class TweetActivity extends AppCompatActivity
{
    @Bean
    TwitterUtils twitterUtils;
    LoadToast lt;
    @ViewById(R.id.picture1)
    ImageView picture1;
    @ViewById(R.id.picture2)
    ImageView picture2;
    @ViewById(R.id.picture3)
    ImageView picture3;
    @ViewById(R.id.picture4)
    ImageView picture4;
    @ViewById(R.id.editText3)
    BootstrapEditText text;
    @ViewById(R.id.textView_num)
    TextView tv;
    @ViewById(R.id.button3)
    BootstrapButton pic_bt;
    @ViewById(R.id.button4)
    FontAwesomeText bt;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    final long[] mPicsId =new long[1];


    @AfterViews
    void init()
    { toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        finish();
        }
    });
    }
    @Click(R.id.button4)
     void launchChooser() {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {   intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);}
        else {intent.setAction(Intent.ACTION_GET_CONTENT);}
        intent.setType(getString(R.string.image_type));
        this.startActivityForResult(intent, 0);
    }

    @Click(R.id.button3)
    void Tweet() {
        lt = new LoadToast(this);
        tweet_show(text.getText().toString());
    }
    @TextChange(R.id.editText3)
    void onTextChangesOnHelloTextView(CharSequence text, TextView hello, int before, int start, int count) {
        this.text.setState(BootstrapEditText.TextState.DEFAULT);
        // 入力文字数の表示
        int txtLength = this.text.getText().toString().codePointCount(0, this.text.length());
        tv.setText(Integer.toString(txtLength) + "/140");
        // 指定文字数オーバーで文字色を赤くする
        int textColor = Color.GRAY;
        if (txtLength > 140) {
            this.text.setState(BootstrapEditText.TextState.DANGER);
            textColor = Color.RED;
            pic_bt.setBootstrapButtonEnabled(false);
        }
        tv.setTextColor(textColor);
    }

    //ツイート
    @UiThread
    void tweet_show(String txt) {
        lt.setText(getString(R.string.Sending));
        lt.show();
        tweet_send(txt);
    }

    @Background
    void tweet_send(String txt) {
        StatusUpdate statusUpdate=new StatusUpdate(txt);
        try {
        if(picture1.getTag()!=null)
        {
           String uri=(String)picture1.getTag();
            UploadedMedia media1 =  Frytter.main_Twitter.uploadMedia(new File(uri));
            mPicsId[0]=media1.getMediaId();
            statusUpdate.setMediaIds(mPicsId);
        }
            else{twitterUtils.showToast(this,"null");}

            Frytter.main_Twitter.updateStatus(statusUpdate);
            isLoadingToast(true);
        } catch (Exception e) {
          e.printStackTrace();
            isLoadingToast(false);
        }}

    @UiThread
    void isLoadingToast(boolean show) {
        if (show) {
            lt.success();
        } else {
            lt.error();
        }
    }


    @OnActivityResult(0)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String path=null;
            Cursor cursor;
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT)
            {
                String[] columns = { MediaStore.Images.Media.DATA };
               cursor =getContentResolver().query(uri, columns, null, null, null);
            }
            else
            {
                int takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
                getContentResolver().takePersistableUriPermission(uri, takeFlags);
                String id = DocumentsContract.getDocumentId(uri);
                String selection = "_id=?";
                String[] selectionArgs = {id.split(":")[1]};
               cursor = getContentResolver().query(
                       Media.EXTERNAL_CONTENT_URI,
                        new String[]{MediaColumns.DATA},
                        selection, selectionArgs, null);
            }

            if(cursor!=null) {
                cursor.moveToFirst();
                path = cursor.getString(0);
                cursor.close();
            }
            try {
                InputStream is = getContentResolver().openInputStream(uri);
                if (picture1.getDrawable() == null) {
                    picture1.setImageDrawable(new BitmapDrawable(getResources(), BitmapFactory.decodeStream(is)));
                    picture1.setTag(path);
                    picture1.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            picture1.setImageDrawable(null);
                            picture1.setTag(null);
                            return false;
                        }
                    });
                }
                else if(picture2.getDrawable() == null)
                {
                    picture2.setImageDrawable(new BitmapDrawable(getResources(), BitmapFactory.decodeStream(is)));
                    picture2.setTag(path);
                    picture2.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            picture2.setImageDrawable(null);
                            picture2.setTag(null);
                            return false;
                        }
                    });
                }
                else if(picture3.getDrawable() == null)
                {
                    picture3.setImageDrawable(new BitmapDrawable(getResources(), BitmapFactory.decodeStream(is)));
                    picture3.setTag(path);
                    picture3.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            picture3.setImageDrawable(null);
                            picture3.setTag(null);
                            return false;
                        }
                    });
                }
                else if(picture4.getDrawable() == null)
                {
                    picture4.setImageDrawable(new BitmapDrawable(getResources(), BitmapFactory.decodeStream(is)));
                    picture4.setTag(path);
                    picture4.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            picture4.setImageDrawable(null);
                            picture4.setTag(null);
                            return false;
                        }
                    });
                }
                is.close();
            }
            catch (IOException ioEx)
            {
                ioEx.printStackTrace();
            }
        }
    }




}



