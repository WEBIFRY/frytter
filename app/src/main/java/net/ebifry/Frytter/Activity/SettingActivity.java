package net.ebifry.Frytter.Activity;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import net.ebifry.Frytter.Declaration.SharedPrefs_;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.Fragments.AccountsManagerFragment_;
import net.ebifry.Frytter.Fragments.DataManager_;
import net.ebifry.Frytter.Fragments.MutedUserFragment_;
import net.ebifry.Frytter.Fragments.SettingFragments_;
import net.ebifry.Frytter.Fragments.ToolsFragment;
import net.ebifry.Frytter.Fragments.ToolsFragment_;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

@EActivity(R.layout.activity_setting)
public class SettingActivity extends AppCompatActivity implements ToolsFragment.OnMenuItemSelectedListener{
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @Pref
    SharedPrefs_ myPrefs;
    @StringRes(R.string.setting_activity_selector_preference)
    String preferencesSelector;
    @AfterViews
    void setChange() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getFragmentManager().beginTransaction().add(R.id.container, ToolsFragment_.builder().build(), "fragment_setting").commit();
    }

    @OnActivityResult(0)
    void onResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                int takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
                this.getContentResolver().takePersistableUriPermission(uri, takeFlags);
            }
            myPrefs.Background().put(data.getData().toString());
            Frytter.activity.setBack(data.getData().toString());
        }
    }



    @Override
    public void onMenuItemSelected(int position, String text)
    {

        FragmentTransaction fragmentTransaction=getFragmentManager()
                .beginTransaction();
        fragmentTransaction.setCustomAnimations(
            android.R.animator.fade_in,
            android.R.animator.fade_out,
            android.R.animator.fade_in,
            android.R.animator.fade_out);
        switch (text) {
            case "デザイン":

                fragmentTransaction
                        .replace(R.id.container, SettingFragments_.builder().string(getString(R.string.setting_activity_selector_design)).build(), "fragment_setting_notification")
                        .addToBackStack(null)
                        .commit();

                break;
            case "通知":
                fragmentTransaction
                        .replace(R.id.container, SettingFragments_.builder().string(getString(R.string.setting_activity_selector_notification)).build(), "fragment_setting_notification")
                        .addToBackStack(null)
                        .commit();

                break;

            case "ミュート":
                fragmentTransaction
                        .replace(R.id.container, MutedUserFragment_.builder().build(), "fragment_setting_mute")
                        .addToBackStack(null)
                        .commit();
                break;
            case "アカウント管理":

                fragmentTransaction
                        .replace(R.id.container, AccountsManagerFragment_.builder().build(), "fragment_setting")
                        .addToBackStack(null)
                        .commit();
                break;
            case "データ":
                fragmentTransaction
                        .replace(R.id.container, DataManager_.builder().build(), "fragment_data_manager")
                        .addToBackStack(null)
                        .commit();
                break;


        }
    }
    @Override
    public void onBackPressed() {
        int backStackCnt = getFragmentManager().getBackStackEntryCount();
        if (backStackCnt != 0) {
            getFragmentManager().popBackStack();
       }
        else finish();
    }




}
