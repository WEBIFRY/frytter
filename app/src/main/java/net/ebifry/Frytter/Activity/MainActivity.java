package net.ebifry.Frytter.Activity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.bumptech.glide.Glide;

import net.ebifry.Frytter.Adapter.PageAdapter;
import net.ebifry.Frytter.Declaration.CircleTransform;
import net.ebifry.Frytter.Declaration.SharedPrefs_;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.Frytter_;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Receive.StreamIntentService;
import net.ebifry.Frytter.Receive.StreamIntentService_;
import net.ebifry.Frytter.Utils.OAuthUtils;
import net.ebifry.Frytter.Utils.TwitterUtils;
import net.ebifry.Frytter.model.DBAccount;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.InputStream;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    ImageView profile_banner;
    ImageView icon;
    @SystemService
    InputMethodManager inputMethodManager;
    @Pref
    SharedPrefs_ myPrefs;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @ViewById(R.id.navigation)
    NavigationView navigationView;
    @ViewById(R.id.pager)
    ViewPager viewBy;
    @ViewById(R.id.back)
    RelativeLayout layout ;
    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bean
    OAuthUtils oauthUtils;
    @ViewById(R.id.editText_status)
    BootstrapEditText tweet;
    @Bean
    TwitterUtils twitterUtils;
    Realm realm;
    //テキスト
    @ViewById(R.id.editText_status)
    BootstrapEditText text;
    @LongClick(R.id.imageButton_Tweet)
    void Long() {
        TweetActivity_.intent(this).start();
    }
    @Click(R.id.imageButton_Tweet)
    void Click() {
        if (text.getText().length() > 0 && getCurrentFocus() != null) {
            twitterUtils.Tweet(this, text.getText().toString());
            text.getEditableText().clear();
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    @AfterViews
   void initViews() {
        toolbar.inflateMenu(R.menu.search);
        //ドロワーヘッダ初期化
        View headerLayout = navigationView.inflateHeaderView(R.layout.drawer_header);
        Spinner screenName_Spinner=(Spinner) headerLayout.findViewById(R.id.screenName_spinner);
         profile_banner =(   ImageView) headerLayout.findViewById(R.id.profile_banner_me);
         icon =(   ImageView) headerLayout.findViewById(R.id.icon_me);

        boolean hasNoKeys = (myPrefs.ConsumerKey().getOr(null) == null && myPrefs.ConsumerSecret().getOr(null) == null);
        Frytter_.activity=this;
        boolean isInit = myPrefs.Initial().getOr(true);
        //認証情報をデータベースに保存していれば
        if (isInit) {
            if (hasNoKeys) {
                InitialActivity_.intent(this).start();
                finish();
            } else {
                InitialActivity_.intent(this).extra("hasKey", true).start();
                finish();
            }
        } else if (StreamIntentService_.TL == null) {
            Frytter.realm = Realm.getInstance(getApplicationContext());
            Frytter.sharedPreferences = (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
            StreamIntentService_.intent(getApplication()).TL().start();
        }

        viewBy.setAdapter(new PageAdapter(getSupportFragmentManager()));
        tweet.setHint(getResources().getString(R.string.What_is_happening));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });


if(myPrefs.ConsumerKey().getOr(null)!=null) {

    //アカウントリストを保存
    AbstractList<Twitter> ac = new ArrayList<>();
    realm = Realm.getInstance(getApplication());
    List<DBAccount> list = realm.where(DBAccount.class).findAll();
    for (DBAccount i : list) {

        Twitter A = oauthUtils.getTwitterInstances(getApplicationContext(), i.getName());
        ac.add(A);
    }
    setBack(myPrefs.Background().getOr(null));
    Frytter.accountList = (ac);
    //ツイッターインスタンス
    Frytter.main_Twitter = (oauthUtils.getTwitterInstance(getApplication()));
    StreamIntentService.stream = (oauthUtils.getTwitterStreamInstance(Frytter.main_Twitter));
    //サイドドロワー初期化
    SetMyName();
    setSideDrawerSwitch(this);
    //スピナー
    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item);
    for (DBAccount i : list) {
        arrayAdapter.add(i.getName());
    }

    //スピナーリスナー
    screenName_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner spinner = (Spinner) parent;
            if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                String item = (String) spinner.getItemAtPosition(position);
                DBAccount account  = realm.where(DBAccount.class).equalTo("Name", item).findFirst();
                myPrefs.Main().put(account.getID());
                Toast.makeText(getApplicationContext(),String.format("%sをメインに設定しました,再起動して下さい",item) , Toast.LENGTH_SHORT).show();
                realm.close();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    });

    toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {

            return false;
        }
    });

    screenName_Spinner.setAdapter(arrayAdapter);

}


    }

    //分岐
    void setSideDrawerSwitch(final Context context)
    {
       navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
           @Override
           public boolean onNavigationItemSelected(MenuItem menuItem) {

               switch (menuItem.getItemId()) {

                   case R.id.action_settings:
                       SettingActivity_.intent(context).start();
                       drawerLayout.closeDrawers();
                       return true;
                   case R.id.action_my_profile:
                       UserActivity_.intent(context).userID(Frytter.myName).start();
                       drawerLayout.closeDrawers();
                       return true;

               }

               return false;
           }
       });
    }
    @Background
    void SetMyName(){
        try {
            N(Frytter.main_Twitter.verifyCredentials());
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }
    @UiThread
    void N(User  user){
        Glide.with(getApplicationContext()).load(user.getBiggerProfileImageURL()).transform(new CircleTransform(getApplicationContext())).into(icon);
        Glide.with(getApplicationContext()).load(user.getProfileBannerMobileURL()).into(profile_banner);
        Frytter.myName = user.getScreenName();
        toolbar.setTitle(Frytter_.myName);
    }

    @Override
    protected void onDestroy() {
        if (Frytter.realm != null && this.realm != null) {
            Frytter.realm.close();
            this.realm.close();
        }

        super.onDestroy();
    }




    //背景設定
   public void setBack(String stringUri)
    {
        if(stringUri==null){
            layout.setBackground(null);}
        InputStream is;
        try{
           is =getContentResolver().openInputStream(Uri.parse(stringUri));
            layout.setBackground(new BitmapDrawable(getResources(), BitmapFactory.decodeStream(is)));
            if(is!=null){
                is.close();
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
