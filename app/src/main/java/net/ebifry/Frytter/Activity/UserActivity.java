package net.ebifry.Frytter.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;

import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.Fragments.TL_;
import net.ebifry.Frytter.Fragments.UserDetailFragment_;
import net.ebifry.Frytter.Fragments.UserListFragment_;
import net.ebifry.Frytter.Fragments.UserListsFragment_;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Utils.TwitterUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;


import twitter4j.Twitter;
import twitter4j.TwitterException;

@EActivity(R.layout.activity_user)
public class UserActivity extends AppCompatActivity {
    @Bean
    TwitterUtils twitterUtils;
    @ViewById(R.id.container)
    RelativeLayout relativeLayout;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;
    @Extra("UserName")
    String userID;


    @AfterViews
    void afv() {
        getFragmentManager().beginTransaction().add(R.id.container, UserDetailFragment_.builder().UserID(userID).build(), "fragment_setting").commit();
        toolbar.setTitle(userID);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    @Background
    public void reply(final String con) {
        for (Twitter t : Frytter.accountList) {
            try {
                t.updateStatus(con);
            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }
    }


    public void fragmentReplacer(String mode,String screeName)
    {
        toolbar.setSubtitle(mode);

        switch (mode)
        {
            case "Favorite":

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, TL_.builder().source(screeName).type(mode).build(), "fragment_following")
                        .addToBackStack("")
                        .setCustomAnimations( android.R.anim.fade_in, android.R.anim.fade_out)
                        .commit();
                break;
            case "Tweet":

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, TL_.builder().source(screeName).type(mode).build(), "fragment_following")
                        .addToBackStack("")
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .commit();
                break;
            case "Following":

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, UserListFragment_.builder().Source(screeName).Mode(mode).build(), "fragment_following")
                        .addToBackStack("")
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .commit();
                break;
            case "Followed":

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, UserListFragment_.builder().Source(screeName).Mode(mode).build(), "fragment_following")
                        .addToBackStack("")
                        .setCustomAnimations( android.R.anim.fade_in, android.R.anim.fade_out)
                        .commit();
                break;
            case "List":

                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, UserListsFragment_.builder().Source(screeName).build(), "fragment_following")
                        .addToBackStack("")
                        .setCustomAnimations(
                                android.R.animator.fade_in,
                                android.R.animator.fade_out,
                                android.R.animator.fade_in,
                                android.R.animator.fade_out)
                        .commit();
                break;
        }
    }
    @Override
    public void onBackPressed() {
        int backStackCnt = getFragmentManager().getBackStackEntryCount();
        int backSupportStackCnt = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackCnt != 0) {
            getFragmentManager().popBackStack();
        }
        if (backSupportStackCnt != 0) {
            getSupportFragmentManager().popBackStack();
        }
        if(backSupportStackCnt == 0&&backStackCnt == 0)finish();
    }

}
