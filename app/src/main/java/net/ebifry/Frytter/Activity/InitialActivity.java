package net.ebifry.Frytter.Activity;

import android.support.v7.app.AppCompatActivity;

import net.ebifry.Frytter.Fragments.Consumer_;
import net.ebifry.Frytter.Fragments.TwitterOAuth_;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

@EActivity(R.layout.activity_initial)
public class InitialActivity extends AppCompatActivity {
    @Extra("hasKey")
    boolean hasKey;
    @AfterExtras
    void set() {
        if (hasKey) {
            OAuthSuccess();
        }
    }
    @AfterViews
    void start() {
        getFragmentManager().beginTransaction().add(R.id.initial_container, Consumer_.builder().build(), "fragment_consumer").commit();
    }

    public void OAuthSuccess() {
        TwitterOAuth_.builder().build().show(getFragmentManager(), "");
    }

}
