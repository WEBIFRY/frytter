package net.ebifry.Frytter.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.bumptech.glide.Glide;

import net.ebifry.Frytter.Declaration.CircleTransform;
import net.ebifry.Frytter.Frytter;
import net.ebifry.Frytter.Fragments.PictureFragment_;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Utils.TwitterUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Locale;

import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.TwitterException;

@EActivity(R.layout.activity_any_user_tweet)
public class AnyUserTweet extends AppCompatActivity {
    private final CircleTransform transformation = new CircleTransform(this);
    @Bean
    TwitterUtils twitterUtils;
    @ViewById(R.id.media_container)
    GridLayout gridLayout;
    @SystemService
    InputMethodManager inputMethodManager;
    @ViewById(R.id.icon)
    ImageView icon;
    @ViewById(R.id.textView_UserName)
    TextView UserName;
    @ViewById(R.id.textView_screenName)
    TextView screenName;
    @ViewById(R.id.textView_text)
    TextView text;
    @ViewById(R.id.textView_fav)
    TextView fav;
    @ViewById(R.id.textView_via)
    TextView via;
    @ViewById(R.id.textView_rt)
    TextView rt;
    @ViewById(R.id.textView_date)
    TextView date;
    @Extra("StatusID")
    long statusID;
    @ViewById(R.id.editText_status)
    BootstrapEditText reply;
    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    private Status status_;
    @Background
    @AfterViews
    void AfterView() {
        try {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            Status status = Frytter.main_Twitter.showStatus(statusID);
            SetItem(status);
        } catch (TwitterException e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
        }
    }

    @Click(R.id.imageButton_Tweet)
    final void ReplySender()
    {
        if (getCurrentFocus() != null) { inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);}
        twitterUtils.ReplyTweet(this, status_.getId(), reply.getText().toString());
        reply.getEditableText().clear();

    }

    @UiThread
    void SetItem(Status status) {
        reply.setHint(String.format("%sさんに返信",status.getUser().getName()));
        status_=status;
        if(status.getExtendedMediaEntities().length>0)
        {
            int i=0;
            for (final MediaEntity media:status.getExtendedMediaEntities()) {
                ImageView img = new ImageView(getApplicationContext());
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                img.setPadding(5, 5, 5, 5);
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(@NonNull View view) {
                        PictureFragment_.builder().URL(media.getMediaURLHttps()).build().show(getSupportFragmentManager(), "");
                    }
                });
                gridLayout.addView(img, i, new GridLayout.LayoutParams(new ViewGroup.LayoutParams(320, 180)));
              Glide.with(getApplicationContext()).load(media.getMediaURLHttps()).into(img);
                i++;
            }
            gridLayout.setVisibility(View.VISIBLE);
        }
        else{gridLayout.setVisibility(View.GONE);}
        Glide.with(getApplicationContext()).load(status.getUser().getOriginalProfileImageURLHttps()).transform(transformation).into(icon);
        UserName.setText(status.getUser().getName());
        screenName.setText(getString(R.string.ScreenName, status.getUser().getScreenName()));
        via.setText(Html.fromHtml(status.getSource()));
        text.setText(status.getText());
        fav.setText(String.format("%s:%d", getString(R.string.favorite), status.getFavoriteCount()));
        rt.setText(String.format("%s:%d", getString(R.string.retweet) , status.getRetweetCount()));
        SimpleDateFormat format = new SimpleDateFormat(getString(R.string.time_set), Locale.JAPAN);
        date.setText(format.format(status.getCreatedAt().getTime()));
    }
    @FocusChange(R.id.editText_status)
    final void Click()
    {
               reply.append(String.format("%s \n",getString(R.string.ScreenName, status_.getUser().getScreenName())));
            reply.setMinLines(5);
    }


}
