package net.ebifry.Frytter.Declaration;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultLong;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;
import org.androidannotations.annotations.sharedpreferences.SharedPref.Scope;

@SharedPref(value= Scope.UNIQUE)
public interface SharedPrefs {
    @DefaultString("null")
    String ConsumerKey();
    @DefaultString("null")
    String ConsumerSecret();
    @DefaultLong(1)
    long Main();
    @DefaultString("null")
    String Background();
    @DefaultBoolean(true)
    boolean Initial();
}
