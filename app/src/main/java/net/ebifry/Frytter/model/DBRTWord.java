package net.ebifry.Frytter.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class DBRTWord extends RealmObject {

    @PrimaryKey
    private String RT;

    public String getRT() {
        return RT;
    }

    public void setRT(String RT) {
        this.RT = RT;
    }


}
