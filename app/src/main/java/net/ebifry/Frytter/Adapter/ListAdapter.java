package net.ebifry.Frytter.Adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.ebifry.Frytter.Declaration.CircleTransform;
import net.ebifry.Frytter.R;

import twitter4j.UserList;

public class ListAdapter extends ArrayAdapter<UserList>{
    final LayoutInflater mInflater;
    public ListAdapter(Context context) {
        super(context, R.layout.list_item_userlist);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }
    public static class ViewHolder {
        public TextView listName;
        public TextView description;
        public TextView followThisList;
        public ImageView icon;
        public CircleTransform transform;
    }

    @Override
    public View getView(int position, View convertview, ViewGroup parent) {

       View view  =convertview ;
        ViewHolder holder;
        UserList item = getItem(position);
        if (view == null) {
            view = mInflater.inflate(R.layout.list_item_userlist,new RelativeLayout(getContext()));
            holder = new ViewHolder();
            holder.transform= new CircleTransform(getContext());
            holder.listName=(TextView)view.findViewById(R.id.list_name);
            holder.description=(TextView)view.findViewById(R.id.Description);
            holder.followThisList=(TextView)view.findViewById(R.id.follow_this_list);
            holder.icon=(ImageView)view.findViewById(R.id.icon);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Glide.with(getContext())
                .load(item.getUser().getOriginalProfileImageURLHttps())
                .transform(holder.transform)
                .into(holder.icon);
        holder.listName.setText(item.getName());
        holder.description.setText(item.getDescription());
        String e=String.format("追加されているメンバー(%d),購読のメンバー(%d)",item.getMemberCount(),item.getSubscriberCount());
        holder.followThisList.setText(e);

        return view;
    }
}
