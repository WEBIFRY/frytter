package net.ebifry.Frytter.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.ebifry.Frytter.Declaration.CircleTransform;
import net.ebifry.Frytter.Fragments.PictureFragment_;
import net.ebifry.Frytter.R;

import org.androidannotations.annotations.SystemService;

import twitter4j.User;


public class UserAdapter extends ArrayAdapter<User>
{


    final LayoutInflater mInflater;
    public UserAdapter(Context context) {
        super(context, R.layout.list_item_user);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder {
        public TextView UserName;
        public TextView Description;
        public TextView ScreenName;
        public ImageView Icon;
        public CircleTransform Transform;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder ;
      final  User item = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_user,new RelativeLayout(getContext()));
            holder = new ViewHolder();
            holder.Transform= new CircleTransform(getContext());
            holder.UserName=(TextView)convertView.findViewById(R.id.UserName);
            holder.Description=(TextView)convertView.findViewById(R.id.Description);
            holder.ScreenName=(TextView)convertView.findViewById(R.id.ScreenName);
            holder.Icon=(ImageView)convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Glide.with(getContext())
                .load(item.getOriginalProfileImageURLHttps())
                .transform(holder.Transform)
                .into(holder.Icon);
        holder.UserName.setText(item.getName());
        holder.ScreenName.setText(getContext().getString(R.string.ScreenName,item.getScreenName()));
        holder.Description.setText(item.getDescription());




        return convertView;
    }



}
