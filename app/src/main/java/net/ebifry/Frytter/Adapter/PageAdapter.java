package net.ebifry.Frytter.Adapter;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import net.ebifry.Frytter.Fragments.TL;
import net.ebifry.Frytter.Fragments.TL_;
import net.ebifry.Frytter.Receive.StreamIntentService;

public class PageAdapter extends FragmentStatePagerAdapter
{
    public PageAdapter(FragmentManager fm)
    { super(fm); }
    @Nullable
    @Override
    public Fragment getItem(int i)
    {
        switch(i)
    {
        case 0:{

            TL u =  TL_.builder().type("Home").build();
            StreamIntentService.TL =u;
            return u;}
        case 1:
        {

            return  TL_.builder().type("Me").build();
        }

        case 2:
        {
            return TL_.builder().type("Mention").build();
        }
         default:return null;


    }
    }
    @Override public int getCount()
{
    return 3;
}

    @Override
      public CharSequence getPageTitle(int position)
{
    return "";
}

    public Fragment findFragmentByPosition(ViewPager viewPager, int position) {
        return (Fragment) instantiateItem(viewPager, position);
    }

}
