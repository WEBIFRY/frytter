package net.ebifry.Frytter.Adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;

import net.ebifry.Frytter.Activity.AnyUserTweet_;
import net.ebifry.Frytter.Activity.UserActivity_;
import net.ebifry.Frytter.Declaration.CircleTransform;
import net.ebifry.Frytter.Declaration.TextLinker;
import net.ebifry.Frytter.Fragments.PictureFragment_;
import net.ebifry.Frytter.Fragments.VideoViewFragment_;
import net.ebifry.Frytter.R;
import net.ebifry.Frytter.Utils.TwitterUtils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Status;


public class TweetAdapter extends ArrayAdapter<Status> {
    public static FragmentManager fm;
    public static TwitterUtils twitterUtils_;
    final LayoutInflater mInflater;
    final Context context;
    public TweetAdapter(Context context_) {
        super(context_, R.layout.list_item_tweet);
        context = context_;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        Status item = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_tweet,new RelativeLayout(getContext()));
            holder = new ViewHolder();
       //     holder.awesomeTextFav = (FontAwesomeText) convertView.findViewById(R.id.fav_awesome);
         //   holder.awesomeTextRT = (FontAwesomeText) convertView.findViewById(R.id.retweet_awesome);
            holder.viewParam = new ViewGroup.LayoutParams(320, 180);
            holder.mimeType = new String[1];
            holder.cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            holder.medias = (GridLayout) convertView.findViewById(R.id.media_container);
            holder.Date = (TextView) convertView.findViewById(R.id.textView_date);
            holder.transform =new  CircleTransform(getContext());
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.RT = (TextView) convertView.findViewById(R.id.textView_isRT);
            holder.via = (TextView) convertView.findViewById(R.id.textView_via);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.screenName = (TextView) convertView.findViewById(R.id.screen_name);
            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.background =(RelativeLayout)convertView.findViewById((R.id.tweet_back));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //二回目も使う
        //isRT
        if (item.isRetweet()) {
            holder.RT.setVisibility(View.VISIBLE);
            holder.RT.setText("RT!" + getContext().getString(R.string.ScreenName, item.getUser().getScreenName()));
            item = item.getRetweetedStatus();
        } else {
            holder.RT.setVisibility(View.INVISIBLE);
        }
      //動かすな
        final Status finalItem = item;
        String text = item.getText();

        //media
        MediaEntity[] exMedia = item.getExtendedMediaEntities();
        if (exMedia.length >0 ) {
            if (holder.medias.getChildCount() > 0 || holder.medias.getChildAt(0) != null) {
                holder.medias.removeAllViews();
            }
            int i = 0;
            for (final MediaEntity media:exMedia) {
                ImageView img = new ImageView(getContext());
                img.setPadding(5, 5, 5, 5);
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                text = text.replace(media.getURL(), "");
                holder.medias.addView(img, i, new GridLayout.LayoutParams(holder.viewParam));
                final  String imgUrl=media.getMediaURLHttps();
                //写真の場合
                    img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(@NonNull View view) {
                            Log.d("url",imgUrl);
                            PictureFragment_.builder().URL(imgUrl).isGif(false).build().show(fm, "");
                        }

                    });

                Glide.with(getContext()).load(imgUrl).into(img);
                i++;
                holder.medias.setVisibility(View.VISIBLE);
            }
        } else {
            holder.medias.setVisibility(View.GONE);
        }
        View.OnClickListener onClickListener=new View.OnClickListener() {
            String[] items=getContext().getResources().getStringArray(R.array.tweet);
            @Override
            public void onClick(View v) {
                final long id=finalItem.getId();
                final AlertDialog.Builder listDlg = new AlertDialog.Builder(context);
                listDlg.setTitle("Tweet");
                listDlg.setItems(
                        items,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String selected=items[which];
                                switch (selected)
                                {
                                    case "詳細":
                                        AnyUserTweet_.intent(getContext()).statusID(id).start();
                                        break;
                                }
                            }
                        });

                // 表示
                listDlg.create().show();
            }
        };
        holder.background.setOnClickListener(onClickListener);
//タッチ
        holder.text.setOnClickListener(onClickListener);
        holder.text.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(@NonNull View v) {
                ClipData.Item clipBoardItem = new ClipData.Item(finalItem.getText());
                holder.mimeType[0] = ClipDescription.MIMETYPE_TEXT_URILIST;
                ClipData cd = new ClipData(new ClipDescription("text_data", holder.mimeType), clipBoardItem);
                holder.cm.setPrimaryClip(cd);
                Toast.makeText(getContext(), "本文をコピーしました", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        //UserName
        holder.name.setText(item.getUser().getName());
        holder.screenName.setText(getContext().getString(R.string.ScreenName,item.getUser().getScreenName()));
        //icon
        Glide.with(getContext())
                .load(item.getUser().getOriginalProfileImageURLHttps())
                .transform(holder.transform)
                .into(holder.icon);
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                UserActivity_.intent(getContext()).userID(finalItem.getUser().getScreenName()).start();
            }
        });
        holder.Date.setText(getRelativeTime(item.getCreatedAt()));
        //isReply
        int i=0;
        final SparseArray<String> links = new SparseArray<>();
        for (HashtagEntity hashtag: item.getHashtagEntities()) {
            links.append(i, "#"+hashtag.getText());
            i++;
        }
        TextLinker.OnLinkClickListener listener = new TextLinker.OnLinkClickListener() {
            @Override
            public void onLinkClick(int textId) {
           Toast.makeText(getContext(),links.get(textId),Toast.LENGTH_LONG).show();
            }
        };

        holder.text.setText(TextLinker.getLinkableText(text, links, listener));
        holder.text.setMovementMethod(LinkMovementMethod.getInstance());
        convertView.setTag(R.string.tag_id,item.getId());
        return convertView;
    }

    String getRelativeTime(Date create) {
        String result;
        long datetime1 = System.currentTimeMillis();
        long datetime2 = create.getTime();
        long Difference = datetime1 - datetime2;
        if (Difference <  60000L) {

            result = String.format(  "%d秒前",TimeUnit.MILLISECONDS.toSeconds(Difference));
        } else if (Difference <  3600000L) {
            result =String.format(  "%d分前",TimeUnit.MILLISECONDS.toMinutes(Difference));
        } else if (Difference < 86400000L) {
            result = String.format(  "%d時間前",TimeUnit.MILLISECONDS.toHours(Difference));
        } else{
            result = String.format(  "%d日前",TimeUnit.MILLISECONDS.toDays(Difference));
        }
        return result;

    }


    public static class ViewHolder {
        public CircleTransform transform;
        public ClipboardManager cm;
        public String[] mimeType;
        public ViewGroup.LayoutParams viewParam;
        public TextView RT;
        public GridLayout medias;
        public TextView via;
        public TextView Date;
        public TextView text;
        public TextView screenName;
        public TextView name;
        public ImageView icon;
        public RelativeLayout background;
    }


}